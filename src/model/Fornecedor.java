package model;

public class Fornecedor extends Thread{
	
	
	private ContaCorrente conta;
	
	public Fornecedor(ContaCorrente conta){
		this.conta = conta;
	}
	
	@Override
	public void run(){
		int contador = 100;
		
		for(int i = 0;i<contador;i++){
				
			try{
				conta.sacar(100);
			}
			catch(ExcecaoSaque ex){
				System.out.println(ex.getMessage());
			}
			
		}
		System.out.println("Fim dos Saques");
		
	}

}
