package model;

public class Banco {

	
	public static void main(String[] args) {
		
		ContaCorrente conta = new ContaCorrente();
		Fornecedor fornecedor1 = new Fornecedor(conta);
		Cliente cliente1 = new Cliente(conta);
		
		System.out.println("Saldo Inicial = " + conta.getSaldo());
		
		cliente1.start();
		fornecedor1.start();
		
		
		while(fornecedor1.getState()!=Thread.State.TERMINATED || cliente1.getState()!=Thread.State.TERMINATED)
		{};	
		
		System.out.println("Saldo Final = " + conta.getSaldo());

	}

}
