package model;

public class ContaCorrente {

	private float saldo = 0;
	private float temp;
	
	public float getSaldo() {
		return saldo;
	}
	
	public synchronized void depositar(float valor) {
		temp = getSaldo();
		saldo = temp + valor;
	}
	
	public synchronized void sacar(float valor) throws ExcecaoSaque {
		temp = getSaldo();
		if(temp>=valor){
		saldo = temp -valor;
		}
		else{
		throw new ExcecaoSaque("Saldo insuficiente, o valor que está tentando ser sacado é muito alto!!");	
		}
		
	}
}
